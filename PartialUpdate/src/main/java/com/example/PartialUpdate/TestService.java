package com.example.PartialUpdate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.Optional;

@Service
public class TestService {
    @Autowired
    private TestRepository testRepository;

    public User partiallyUpdate(User user, String userId) {
        final User existingUser = testRepository.findByUserId(userId);
        System.out.println(existingUser.getUsername());

        User updatedUrs = null;
        try {
            updatedUrs = merge(existingUser, user);
            System.out.println(updatedUrs.getUsername());
        } catch (IllegalAccessException e) {
            System.out.println("Exception: " + e);
        } catch (InstantiationException e) {
            System.out.println("Exception: " + e);
        }
        return testRepository.save(updatedUrs);
    }

    public User saveUser(User user) {
        return testRepository.save(user);
    }

    public <T> T merge(T local, T remote) throws IllegalAccessException, InstantiationException {
        Class<?> clazz = local.getClass();
        Object merged = clazz.newInstance();
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            Object localValue = field.get(local);
            Object remoteValue = field.get(remote);
            Object value = (localValue != null) ? localValue : remoteValue;
            field.set(merged, value);
            if (localValue != null) {
                field.set(merged, (remoteValue != null) ? remoteValue : localValue);
            }
        }
        return (T) merged;
    }
}
