package com.example.PartialUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document("User")
public class User {
    @Id
    private String userId;
    private String username;
    private Integer uid;
    private String firstName;
    private String secondName;
    private List<Vehicle> vehicles;

}
