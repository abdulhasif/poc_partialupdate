package com.example.PartialUpdate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    TestService testService;

    @GetMapping("/tst")
    public String testing() {
        return "RUNNING";
    }

    @PostMapping("/")
    public User saveUserData(@RequestBody User user) {
        return testService.saveUser(user);
    }

    @PatchMapping("/{userId}")
    public User updatePartially(@PathVariable("userId") String userId,@RequestBody User user)
    {
        return testService.partiallyUpdate(user,userId);
    }

}
