package com.example.PartialUpdate;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface TestRepository extends MongoRepository<User,String> {
    User findByUserId(String userId);
}
